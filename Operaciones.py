def pedirOpcion():
    correcto = False
    num = 0
    while (not correcto):
        try:
            num = int(input("Digite una opción: "))
            correcto = True
        except ValueError:
            print('Error, digite un número entero')
    return num


def pedirNum1():
    correcto = False
    num1 = 0
    while (not correcto):
        try:
            num1 = int(input("Digite el primer número entero: "))
            correcto = True
        except ValueError:
            print('Error, digite un número entero')
    return num1


def pedirNum2():
    correcto = False
    num2 = 0
    while (not correcto):
        try:
            num2 = int(input("Digite el segundo número entero: "))
            correcto = True
        except ValueError:
            print('Error, digite un número entero')
    return num2


salir = False

while not salir:

    print("")
    print("1. Suma (+)")
    print("2. Resta (-)")
    print("3. Multiplicación (*)")
    print("4. División (/)")
    print("5. Potenciación (^)")
    print("6. SALIR")
    print("")

    opcion = pedirOpcion()

    if opcion == 1:
        print("+")
        a = pedirNum1()
        b = pedirNum2()
        print(f'El resultado de la suma es: {a+b}')
    elif opcion == 2:
        print("-") 

        x = pedirNum1()
        y = pedirNum2()
        z = (x - y)
        print ("resulaltado de la resta", (z))
    elif opcion == 3:
        print("*")
    elif opcion == 4:
        print("/")
    elif opcion == 5:
        print("^")
    elif opcion == 6:
        print("Hasta pronto")
        salir = True
    else:
        print("Digite un número entre 1 y 6")